var EnemyShip = function(argument) {

    var health = 100;
    var speed = 80;
    var position = createVector(width, random(0, height - 64));
    var img = Data.get('enemy_ship');
    var lastTimeFired = 0;


    this.draw = function(delta) {
        image(img, position.x, position.y);
    };

    this.fire = function() {
        var now = millis();
        if (now - lastTimeFired > 1500) {
            var pos = position.copy();
            pos.y += 28;

            scene.createSprite({
                type: 'enemy_bullet',
                position: pos,
                speed: -300
            });
            lastTimeFired = now;
        }
    }

    this.update = function(delta) {
        position.x -= delta / 1000 * speed;
        this.fire();
    };
}