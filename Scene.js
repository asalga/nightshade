var Scene = function() {

    var actors = [];
    var bullets = [];
    var ship;


    this.draw = function() {
        actors.forEach(e => e.draw());
        bullets.forEach(b => b.draw());
        user.draw();
    };

    this.update = function(delta) {
        if (frameCount % 150 == 0) {
            this.createSprite({ type: 'enemy_ship' });
        }

        actors.forEach(e => e.update(delta));
        bullets.forEach(b => b.update(delta));

        user.update(delta);
    };

    this.createSprite = function(cfg) {
        if (cfg.type == 'user_ship') {
            user = new Ship();
        }
        if (cfg.type === 'enemy_ship') {
            actors.push(new EnemyShip());
        }
        if (cfg.type === 'user_bullet') {
            bullets.push(new Bullet(cfg));
        }
        if (cfg.type === 'enemy_bullet') {
            bullets.push(new Bullet(cfg));
        }
    };
}