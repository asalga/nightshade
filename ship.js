function Ship() {

    this.position = createVector(0, height / 2);
    var lastTimeFired = 0;

    this.update = function(delta) {

        var s = 400 * delta / 1000;

        if (keyIsDown(UP_ARROW)) {
            this.position.y -= s;
        }
        if (keyIsDown(DOWN_ARROW)) {
            this.position.y += s;
        }
        if (keyIsDown(LEFT_ARROW)) {
            this.position.x -= s;
        }
        if (keyIsDown(RIGHT_ARROW)) {
            this.position.x += s;
        }

        if (keyIsDown(32)) {
            var now = millis();
            if (now - lastTimeFired > 500) {
                this.fire();
                lastTimeFired = millis();
            }
        }

        this.position.x = constrain(this.position.x, 0, width - 64);
        this.position.y = constrain(this.position.y, 0, height - 64);
    };

    this.fire = function() {
        var pos = this.position.copy();
        pos.y += 28;
        pos.x += 50;
        scene.createSprite({
            type: 'user_bullet',
            position: pos,
            speed: 600
        });
    }


    this.draw = function() {
        image(Data.get('user_ship'), this.position.x, this.position.y);
    };


}