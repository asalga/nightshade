function StarBackground() {

    this.stars = new Array(50);

    for (var i = 0; i < this.stars.length; ++i) {
        this.stars[i] = createVector(
            random(0, width),
            random(0, height),
            random(30, 300)
        );
    }

    this.update = function(d) {
        for (var i = 0; i < this.stars.length; ++i) {
            this.stars[i].x -=  d / 1000 * this.stars[i].z;

            if(this.stars[i].x < - 5){
                this.stars[i].x = random(width, width*2);
                this.stars[i].y = random(0, height);
                this.stars[i].z = random(30, 300);
            }

        }
    };

    this.draw = function() {
        for (var i = 0; i < this.stars.length; ++i) {
            fill(255);
            rect(this.stars[i].x, this.stars[i].y, 4, 4);
        }

    };

}