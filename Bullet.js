// type
// position
// speed
var Bullet = function(cfg) {
    Object.assign(this, cfg);

    this.draw = function() {
        image(Data.get(this.type), this.position.x, this.position.y);
    };

    this.update = function(delta) {
    	 this.position.x += delta/1000 * this.speed;
    };
};