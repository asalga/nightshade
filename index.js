var player;
var enemyShip;

var lastTime = 0;
var now;
var starBackground;
var scene;

function setup() {
    createCanvas(640, 480);

    scene = new Scene();
    scene.createSprite({
        type: 'user_ship'
    });

    starBackground = new StarBackground();
}

function draw() {

    now = millis();
    var timeDifference = now - lastTime;

    scene.update(timeDifference);
    starBackground.update(timeDifference);

    background(0, 0, 0);

    starBackground.draw();
    scene.draw();

    lastTime = now;
}